package com.fishky.discovery;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class ApplicationTest {

    @Test
    void contextLoads() {
        assertEquals(4, 2 + 2);
    }

}
