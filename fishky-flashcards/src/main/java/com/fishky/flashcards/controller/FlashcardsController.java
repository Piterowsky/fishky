package com.fishky.flashcards.controller;

import com.fishky.flashcards.integration.MailingIntegration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlashcardsController {

    private final MailingIntegration mailingIntegration;

    public FlashcardsController(MailingIntegration mailingIntegration) {
        this.mailingIntegration = mailingIntegration;
    }

    @GetMapping("/send")
    public String hello() {
        return mailingIntegration.sendMail("mailing message");
    }

}
