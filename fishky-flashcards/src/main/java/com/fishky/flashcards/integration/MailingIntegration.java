package com.fishky.flashcards.integration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MailingIntegration {

    @Value("${app.fishky-mailing.host}")
    private String host;

    @Value("${app.fishky-mailing.port}")
    private String port;

    private final RestTemplate restTemplate;

    public MailingIntegration(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String sendMail(String message) {
        var url = String.format("%s:%s/send", host, port);
        var body = new HttpEntity<>(message);
        var response = restTemplate.exchange(url, HttpMethod.POST, body, String.class);
        return response.getBody();
    }

}
