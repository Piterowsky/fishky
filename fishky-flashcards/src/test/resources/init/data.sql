INSERT INTO flashcard_sets (user_id, name)
OVERRIDING SYSTEM VALUE
VALUES
(1, 'flashcard-set-1'),
(1, 'flashcard-set-2'),
(1, 'flashcard-set-3'),
(2, 'flashcard-set-1'),
(2, 'flashcard-set-2'),
(2, 'flashcard-set-3'),
(2, 'flashcard-set-4'),
(2, 'flashcard-set-5');
