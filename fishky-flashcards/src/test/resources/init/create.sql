DROP TABLE IF EXISTS flashcards;
DROP TABLE IF EXISTS flashcard_sets;

CREATE TABLE flashcard_sets (
    flashcard_set_id int PRIMARY KEY GENERATED AS IDENTITY,
    user_id bigint NOT NULL,
    name VARCHAR(64) NOT NULL,
    lastReview timestamp,
    UNIQUE(user_id, name)
);

CREATE TABLE flashcards (
    flashcard_id int PRIMARY KEY GENERATED AS IDENTITY,
    term VARCHAR(512) NOT NULL,
    definition VARCHAR(512) NOT NULL,
    lastReview timestamp,
    repetitions int,
    flashcard_set_id bigint,
    FOREIGN KEY(flashcard _set_id) REFERENCES flashcard_sets (flashcard_set_id);
);