#!/bin/bash
FILE_PATH=$(pwd)

cd $FILE_PATH
cd ../../fishky-mailing/scripts/
./build.sh

cd $FILE_PATH
cd ../../fishky-flashcards/scripts/
./build.sh