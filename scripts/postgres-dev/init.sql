-- TODO: Add placeholder for password and inject it from pipeline
CREATE USER fishky_auth WITH PASSWORD 'fishky_auth';
CREATE DATABASE fishky_auth;
GRANT ALL PRIVILEGES ON DATABASE fishky_auth TO fishky_auth;

-- TODO: Add placeholder for password and inject it from pipeline
CREATE USER fishky_flashcards WITH PASSWORD 'fishky_flashcards';
CREATE DATABASE fishky_flashcards;
GRANT ALL PRIVILEGES ON DATABASE fishky_flashcards TO fishky_flashcards;