package com.fishky.auth.error.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class ApiValidationError implements ApiSubError {

    private final String object;
    private final String field;
    private final Object rejectedValue;
    private final String message;

}
