package com.fishky.auth.error;

import com.fishky.auth.error.dto.ApiError;
import com.fishky.auth.error.exception.InvalidRequestException;
import com.fishky.auth.error.exception.NotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings("unused")
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handles all {@link InvalidRequestException} exceptions
     * @param ex exception
     * @return response with {@link ApiError} object based on exception content
     */
    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<ApiError> handleInvalidRequestException(InvalidRequestException ex) {
        var status = HttpStatus.BAD_REQUEST;
        var error = new ApiError(status, ApiErrorType.INVALID_INPUT_ERROR, ex);
        return ResponseEntity.status(status).body(error);
    }

    /**
     * Handles all {@link NotFoundException} exceptions
     * @param ex exception
     * @return response with {@link ApiError} object based on exception content
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiError> handleNotFoundException(NotFoundException ex) {
        var status = HttpStatus.NOT_FOUND;
        var error = new ApiError(status, ApiErrorType.NOT_FOUND_ERROR, ex);
        return ResponseEntity.status(status).body(error);
    }

}
