package com.fishky.auth.error;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiErrorType {

    public static final String INVALID_INPUT_ERROR = "Invalid input";
    public static final String NOT_FOUND_ERROR = "Not found";

}
