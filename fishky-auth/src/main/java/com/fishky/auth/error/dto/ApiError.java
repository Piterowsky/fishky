package com.fishky.auth.error.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fishky.auth.util.InstantUtils;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class ApiError {

    @JsonDeserialize(using = InstantUtils.Deserializer.class)
    @JsonSerialize(using = InstantUtils.Serializer.class)
    private final Instant timestamp;

    private int status;
    private String message;
    private String debugMessage;
    private List<ApiSubError> subErrors = new ArrayList<>();

    private ApiError() {
        timestamp = Instant.now();
    }

    public ApiError(HttpStatus status) {
        this();
        this.status = status.value();
    }

    public ApiError(HttpStatus status, Throwable ex) {
        this();
        this.status = status.value();
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status.value();
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

}
