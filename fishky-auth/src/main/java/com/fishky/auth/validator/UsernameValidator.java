package com.fishky.auth.validator;

import com.fishky.auth.error.exception.InvalidRequestException;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class UsernameValidator {

    public static final int MIN_SIZE = 6;
    public static final int MAX_SIZE = 32;

    public static final String NO_ILLEGAL_CHARACTERS_MESSAGE =
            "Username should not contain special characters except underscore '_'";
    public static final String NO_WHITESPACE_CHARACTERS_MESSAGE =
            "Username should not contain whitespace characters";
    public static final String SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE =
            "Username should has at least " + MIN_SIZE + " characters";
    public static final String SHOULD_HAVE_MAX_CHARACTERS_LENGTH_MESSAGE =
            "Username should has minimum " + MAX_SIZE + " characters";

    @SuppressWarnings("all")
    private static final String ILLEGAL_CHARACTERS_PATTERN = "^.*[^a-zA-Z0-9_]+.*$";
    private static final String WHITESPACES_PATTERN = "^.*\\s.*$";

    public void validate(String username) {
        if(username.length() < MIN_SIZE) {
            throw new InvalidRequestException(SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE);
        }

        if(username.length() > MAX_SIZE) {
            throw new InvalidRequestException(SHOULD_HAVE_MAX_CHARACTERS_LENGTH_MESSAGE);
        }

        if(Pattern.matches(WHITESPACES_PATTERN, username)) {
            throw new InvalidRequestException(NO_WHITESPACE_CHARACTERS_MESSAGE);
        }

        if(Pattern.matches(ILLEGAL_CHARACTERS_PATTERN, username)) {
            throw new InvalidRequestException(NO_ILLEGAL_CHARACTERS_MESSAGE);
        }
    }

}
