package com.fishky.auth.validator;

import com.fishky.auth.error.exception.InvalidRequestException;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class PasswordValidator {

    public static final int MIN_SIZE = 8;

    public static final String AT_LEAST_ONE_LOWERCASE_MESSAGE =
            "Password should contain at least one lowercase letter";
    public static final String AT_LEAST_ONE_UPPERCASE_MESSAGE =
            "Password should contain at least one uppercase letter";
    public static final String AT_LEAST_ONE_NUMBER_MESSAGE =
            "Password should contain at least one number";
    public static final String SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE =
            "Password should contain at least " + MIN_SIZE + " characters";

    private static final String AT_LEAST_ONE_LOWERCASE_LETTER_PATTERN = "^.*[a-z].*$";
    private static final String AT_LEAST_ONE_UPPERCASE_LETTER_PATTERN = "^.*[A-Z].*$";
    private static final String AT_LEAST_ONE_NUMBER_PATTERN = "^.*\\d.*$";

    public void validate(String password) {
        if (password.length() < MIN_SIZE) {
            throw new InvalidRequestException(SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE);
        }

        if (!Pattern.matches(AT_LEAST_ONE_LOWERCASE_LETTER_PATTERN, password)) {
            throw new InvalidRequestException(AT_LEAST_ONE_LOWERCASE_MESSAGE);
        }

        if (!Pattern.matches(AT_LEAST_ONE_UPPERCASE_LETTER_PATTERN, password)) {
            throw new InvalidRequestException(AT_LEAST_ONE_UPPERCASE_MESSAGE);
        }

        if (!Pattern.matches(AT_LEAST_ONE_NUMBER_PATTERN, password)) {
            throw new InvalidRequestException(AT_LEAST_ONE_NUMBER_MESSAGE);
        }
    }

}
