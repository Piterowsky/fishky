package com.fishky.auth.validator;

import org.springframework.stereotype.Component;

@Component
public class CreateUserRequestValidator {

    private final UsernameValidator usernameValidator;
    private final PasswordValidator passwordValidator;
    private final EmailValidator emailValidator;

    public CreateUserRequestValidator(UsernameValidator usernameValidator,
                                      PasswordValidator passwordValidator,
                                      EmailValidator emailValidator) {
        this.usernameValidator = usernameValidator;
        this.passwordValidator = passwordValidator;
        this.emailValidator = emailValidator;
    }

    public void validate(String username, String password, String email) {
        usernameValidator.validate(username);
        passwordValidator.validate(password);
        emailValidator.validate(email);
    }

}
