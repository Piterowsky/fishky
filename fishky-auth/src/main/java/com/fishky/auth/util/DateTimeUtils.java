package com.fishky.auth.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateTimeUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtils.class);

    /**
     * Default format used in system.
     */
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * Gets date in {@link String} format and converts it to {@link Instant}.
     *
     * @param date date string in "yyyy-MM-dd HH:mm:ss" format
     * @return {@link Instant UTC representation of given date {@link String}
     */
    public static Instant parse(String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)).toInstant(ZoneOffset.UTC);
    }

    /**
     * Sets JVM default Time Zone to UTC.
     */
    public static void setDefaultTimeZone() {
        var defaultTimeZone = "UTC";
        TimeZone.setDefault(TimeZone.getTimeZone(defaultTimeZone));
        LOGGER.info("Set up default time zone to ");
    }

}
