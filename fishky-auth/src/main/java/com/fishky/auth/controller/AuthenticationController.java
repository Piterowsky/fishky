package com.fishky.auth.controller;

import com.fishky.auth.service.AuthenticationService;
import org.openapitools.api.AuthApi;
import org.openapitools.model.CreateUserRequest;
import org.openapitools.model.GetAuthTokenRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController implements AuthApi {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public ResponseEntity<String> getAuthToken(GetAuthTokenRequest getAuthTokenRequest) {
        var username = getAuthTokenRequest.getUsername();
        var password = getAuthTokenRequest.getPassword();
        var token = authenticationService.getAuthToken(username, password);
        return ResponseEntity.ok(token);
    }

    @Override
    public ResponseEntity<String> createUser(CreateUserRequest createUserRequest) {
        var username = createUserRequest.getUsername();
        var password = createUserRequest.getPassword();
        var email = createUserRequest.getEmail();
        var createdUser = authenticationService.createUser(username, password, email);
        return ResponseEntity.status(HttpStatus.CREATED).body("Created: " + createdUser.getUsername());
    }

}
