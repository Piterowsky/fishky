package com.fishky.auth.service;

import com.fishky.auth.error.exception.InvalidRequestException;
import com.fishky.auth.error.exception.NotFoundException;
import com.fishky.auth.model.User;
import com.fishky.auth.repository.UserRepository;
import com.fishky.auth.util.JwtUtils;
import com.fishky.auth.validator.CreateUserRequestValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtils jwtUtils;
    private final CreateUserRequestValidator createUserRequestValidator;

    public AuthenticationService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtUtils jwtUtils,
                                 CreateUserRequestValidator createUserRequestValidator) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
        this.createUserRequestValidator = createUserRequestValidator;
    }

    public String getAuthToken(String username, String password) {
        var user = userRepository.findOneByUsername(username)
                .orElseThrow(() -> new NotFoundException("User with given username does not exist"));

        var passwordDoesNotMatch = !passwordEncoder.matches(password, user.getPassword());
        if (passwordDoesNotMatch) {
            log.debug("Password check for {} request is incorrect", username);
            throw new InvalidRequestException("Given credentials are incorrect");
        }
        return jwtUtils.generateToken(username);
    }

    public User createUser(String username, String password, String email) {
        createUserRequestValidator.validate(username, password, email);

        if (userRepository.existsByUsername(username)) {
            var message = String.format("User with username '%s' already exists", username);
            throw new InvalidRequestException(message);
        }

        if (userRepository.existsByEmail(email)) {
            var message = String.format("Email '%s' has already been used to register an account", email);
            throw new InvalidRequestException(message);
        }

        var user = User.builder()
                .username(username)
                .hash(passwordEncoder.encode(password))
                .email(email)
                .build();

        return userRepository.save(user);
    }

}
