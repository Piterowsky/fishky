CREATE TABLE users
(
    id           bigint generated always as identity,
    username     VARCHAR(32)  NOT NULL,
    hash         VARCHAR(60)  NOT NULL,
    email        VARCHAR(128) NOT NULL,
    created_by   VARCHAR(64)  NOT NULL,
    created_date TIMESTAMP    NOT NULL,
    updated_by   VARCHAR(64),
    updated_date TIMESTAMP
);

CREATE INDEX idx_users_username ON users (username);
CREATE INDEX idx_users_email ON users (email);

INSERT INTO users(id, created_by, created_date, updated_by, updated_date, email, hash, username)
    OVERRIDING SYSTEM VALUE
VALUES (1,
        'admin',
        to_timestamp('2021-09-15 23:00:00', 'yyyy-MM-dd HH24:MI:ss'),
        NULL,
        NULL,
        'testtest1234@test.com',
        '$2a$11$.NzbEwMaPamZiakPfYj33eX1CVMdl3qrRPWEkTIIb.RvNM/hCWypu',
        'fishkyTestUser1234'),
       (2,
        'admin',
        to_timestamp('2021-09-14 22:00:00', 'yyyy-MM-dd HH24:MI:ss'),
        NULL,
        NULL,
        'testtest@test.com',
        '$2a$11$ZCJxoBzmD0I6/IaPCMLuwOCPXF2kiTpGbKVU72TjsmcqO5L66wFiC',
        'fishkyTestUser'),
       (3,
        'admin',
        to_timestamp('2021-09-13 21:00:00', 'yyyy-MM-dd HH24:MI:ss'),
        NULL,
        NULL,
        'testtest4321@test.com',
        '$2a$11$IgtJXh/hSF.TmzXW53GRPeJ7Yz.HLjN2lMyZgdOQBYlc.6bh0Njai',
        'fishkyTestUser4321');