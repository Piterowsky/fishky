package com.fishky.auth;

import com.fishky.auth.model.User;
import com.fishky.auth.util.DateTimeUtils;
import org.assertj.core.api.SoftAssertions;

public class UserTestHelper {

    public static User getUser1() {
        var user = new User();
        user.setId(1L);
        user.setUsername("fishkyTestUser1234");
        user.setEmail("testtest1234@test.com");
        user.setHash("$2a$11$.NzbEwMaPamZiakPfYj33eX1CVMdl3qrRPWEkTIIb.RvNM/hCWypu");
        user.setCreatedBy("admin");
        user.setCreatedDate(DateTimeUtils.parse("2021-09-15 23:00:00"));
        user.setUpdatedBy(null);
        user.setUpdatedDate(null);
        return user;
    }

    public static void softlyCompareUsers(User userChecked, User userExpected) {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(userChecked.getUsername()).as("username").isEqualTo(userExpected.getUsername());
            softly.assertThat(userChecked.getEmail()).as("email").isEqualTo(userExpected.getEmail());
            softly.assertThat(userChecked.getHash()).as("hash").isEqualTo(userExpected.getHash());
            softly.assertThat(userChecked.getCreatedBy()).as("createdBy").isEqualTo(userExpected.getCreatedBy());
            softly.assertThat(userChecked.getCreatedDate()).as("createdDate").isEqualTo(userExpected.getCreatedDate());
            softly.assertThat(userChecked.getUpdatedBy()).as("updatedBy").isEqualTo(userExpected.getUpdatedBy());
            softly.assertThat(userChecked.getUpdatedDate()).as("updatedDate").isEqualTo(userExpected.getUpdatedDate());
            softly.assertThat(userChecked.isAccountNonExpired()).as("accountNotExpired")
                    .isEqualTo(userExpected.isAccountNonExpired());
            softly.assertThat(userChecked.isAccountNonLocked()).as("accountNotLocked")
                    .isEqualTo(userExpected.isAccountNonLocked());
            softly.assertThat(userChecked.isEnabled()).as("accountEnabled")
                    .isEqualTo(userExpected.isEnabled());
            softly.assertThat(userChecked.isCredentialsNonExpired()).as("credentialsNotExpired")
                    .isEqualTo(userExpected.isCredentialsNonExpired());
            softly.assertThat(userChecked.getAuthorities().toArray())
                    .containsExactly(userExpected.getAuthorities().toArray());
            softly.assertAll();
        });
    }


}
