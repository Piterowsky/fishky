package com.fishky.auth.repository;

import com.fishky.auth.UserTestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.stream.Stream;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @DisplayName("Find user by username when user exist, username=\"fishkyTestUser1234\"")
    void testFindByUsername() {
        //given
        var username = "fishkyTestUser1234";
        var expectedUser = UserTestHelper.getUser1();

        // when
        var result = userRepository.findOneByUsername(username);

        // then
        Assertions.assertTrue(result.isPresent());
        UserTestHelper.softlyCompareUsers(result.get(), expectedUser);
    }

    @ParameterizedTest(name = "{index}: Exists by username, username=\"{0}\"")
    @MethodSource("testExistsByUsernameProvider")
    void testExistsByUsername(String username, boolean expectedResult) {
        // when
        var result = userRepository.existsByUsername(username);

        // then
        Assertions.assertEquals(expectedResult, result);
    }

    public static Stream<Arguments> testExistsByUsernameProvider() {
        return Stream.of(
                Arguments.of("fishkyTestUser1234", true),
                Arguments.of("fishkyTestUser", true),
                Arguments.of("fishkyTestUser4321", true),
                Arguments.of("fishkytestuser", false),
                Arguments.of("fishkyTestUser1234 ", false),
                Arguments.of(" fishkyTestUser1234", false),
                Arguments.of(" ", false)
        );
    }

}