package com.fishky.auth.util;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Contains matchers for dealing with date time. Consider this class as an extension for hamcrest formatters
 */
public class DateTimeMatchers extends TypeSafeMatcher<String> {

    private final Instant now;
    private final Instant bottomDate;

    private DateTimeMatchers(int seconds) {
        this.now = Instant.now();
        this.bottomDate = now.minus(seconds, ChronoUnit.SECONDS);
    }

    /**
     * Returns matcher which checks if date is in date range
     * @param seconds number seconds from now to bottom date
     * @return matcher
     */
    public static DateTimeMatchers isBetweenLastSeconds(int seconds) {
        return new DateTimeMatchers(seconds);
    }

    @Override
    protected boolean matchesSafely(String date) {
        var instant = DateTimeUtils.parse(date);
        return instant.isAfter(bottomDate) && instant.isBefore(Instant.now());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("is within " + bottomDate + " and " + now);
    }

}
