package com.fishky.auth.util;

import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class ControllerTestUtils {

    /**
     * Tests error api response.
     *
     * @param result       performed action result
     * @param status       http status of response
     * @param message      main error response message
     * @param debugMessage detailed error message
     * @throws Exception thrown when failure occur
     */
    public static void testErrorResponse(ResultActions result, HttpStatus status, String message, String debugMessage)
            throws Exception {
        result
                .andExpect(MockMvcResultMatchers.status().is(status.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.timestamp").value(DateTimeMatchers.isBetweenLastSeconds(5)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(status.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(message))
                .andExpect(MockMvcResultMatchers.jsonPath("$.debugMessage").value(debugMessage));
    }

}
