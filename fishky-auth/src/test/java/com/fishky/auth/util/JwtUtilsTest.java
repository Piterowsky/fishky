package com.fishky.auth.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class JwtUtilsTest {

    // Valid to 18/04/2036
    private static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImV4cCI6MjA5MTgxNTE1NiwiaWF0I" +
            "joxNjE4Nzc1MTU2fQ.xyEvinqwkaAFxiWDOzteqF7tlmKKCOwZiTaaRa6lMe4";

    // Expired 18/04/2021
    private static final String EXPIRED_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImV4cCI6MTYxODc3NDE0M" +
            "CwiaWF0IjoxNjE4Nzc0MTM5fQ.Yrcg_rleUC6NpmML2fSedhij1cRgd7MUhSMAkjtl81Y";

    // Secret "invalidSecret"
    private static final String INVALID_SECRET_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImV4cCI6MTYxOD" +
            "c3NDI2OSwiaWF0IjoxNjE4Nzc0MjY4fQ.dfvdTzzCPTMSRYQOkxWuI16vo73b0aP1brklM1kxxtE";

    private static final Integer VALIDITY_PERIOD_IN_SECONDS = 15 * 60;
    public static final String SECRET = "$3cr3t";

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(jwtUtils, "tokenValidityInSeconds", VALIDITY_PERIOD_IN_SECONDS);
        ReflectionTestUtils.setField(jwtUtils, "tokenSecret", SECRET);
    }

    @Spy
    private JwtUtils jwtUtils;

    @Test
    void validateToken_whenTokenIsValid_returnTrue() {
        var isTokenValid = jwtUtils.validateToken(TOKEN);

        assertTrue(isTokenValid);
    }

    @Test
    void validateToken_whenTokenIsExpired_returnFalse() {
        var isTokenValid = jwtUtils.validateToken(EXPIRED_TOKEN);

        assertFalse(isTokenValid);
    }

    @Test
    void validateToken_whenTokenGeneratedWithInvalidSecret_returnFalse() {
        var isTokenValid = jwtUtils.validateToken(INVALID_SECRET_TOKEN);

        assertFalse(isTokenValid);
    }

    @RepeatedTest(10) // Sometimes acceptable threshold can be too small for local machine depending on computer spec
    void generateToken_whenTokenGenerated_hasValidClaimsAndExpirationTime() {
        var username = "username";
        var acceptableThreshold = 5000;

        var token = jwtUtils.generateToken(username);

        assertEquals(username, jwtUtils.getUsername(token));
        assertTrue(isExpirationDateInAcceptableRange(jwtUtils.getExpirationDate(token), acceptableThreshold));
    }

    private boolean isExpirationDateInAcceptableRange(Date expirationDate, int acceptableThreshold) {
        var fifteenMinutesFromNowTimestamp = System.currentTimeMillis() + VALIDITY_PERIOD_IN_SECONDS * 1000;
        return Math.abs(expirationDate.getTime() - fifteenMinutesFromNowTimestamp) < acceptableThreshold;
    }

}