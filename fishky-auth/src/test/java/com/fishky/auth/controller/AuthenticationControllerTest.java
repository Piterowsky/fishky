package com.fishky.auth.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fishky.auth.error.ApiErrorType;
import com.fishky.auth.error.exception.InvalidRequestException;
import com.fishky.auth.error.exception.NotFoundException;
import com.fishky.auth.model.User;
import com.fishky.auth.service.AuthenticationService;
import com.fishky.auth.util.ControllerTestUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openapitools.model.CreateUserRequest;
import org.openapitools.model.GetAuthTokenRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    private static final String AUTH_TOKEN_URL = "/auth/token";
    private static final String AUTH_CREATE_USER_URL = "/auth/user";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @MockBean
    private AuthenticationService authenticationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetAuthToken_whenCorrect_returnToken() throws Exception {
        var username = "username";
        var password = "password";
        var token = "token";
        var request = createGetAuthTokenRequest(username, password);

        Mockito.doReturn(token).when(authenticationService).getAuthToken(username, password);
        var result = mockMvc.perform(post(AUTH_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(request)));

        result
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").value(token));
    }

    @Test
    void testGetAuthToken_whenThrownInvalidRequestException_return400() throws Exception {
        var username = "username";
        var password = "password";
        var debugMessage = "debug-exception-message";
        var request = createGetAuthTokenRequest(username, password);
        var exception = new InvalidRequestException(debugMessage);

        doThrow(exception).when(authenticationService).getAuthToken(username, password);
        var result = mockMvc.perform(post(AUTH_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(request)));

        ControllerTestUtils.testErrorResponse(result, HttpStatus.BAD_REQUEST, ApiErrorType.INVALID_INPUT_ERROR, debugMessage);
    }

    @Test
    void testGetAuthToken_whenThrownNotFoundException_return404() throws Exception {
        var username = "username";
        var password = "password";
        var debugMessage = "debug-exception-message";
        var request = createGetAuthTokenRequest(username, password);
        var exception = new NotFoundException(debugMessage);

        doThrow(exception).when(authenticationService).getAuthToken(username, password);
        var result = mockMvc.perform(post(AUTH_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(request)));

        ControllerTestUtils.testErrorResponse(result, HttpStatus.NOT_FOUND, ApiErrorType.NOT_FOUND_ERROR, debugMessage);
    }

    @Test
    void testCreateUser_whenCorrect_returnToken() throws Exception {
        var username = "username";
        var password = "password";
        var email = "email";
        var user = User.builder().username(username).hash(password).email(email).build();
        var request = createCreateUserRequest(username, password, email);

        Mockito.doReturn(user).when(authenticationService).createUser(username, password, email);
        var result = mockMvc.perform(post(AUTH_CREATE_USER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(request)));

        result
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").value("Created: " + request.getUsername()));
    }

    @Test
    void testCreateUser_whenServiceThrowInvalidRequestException_return400() throws Exception {
        var username = "username";
        var password = "password";
        var email = "email";
        var debugMessage = "debug-exception-message";
        var request = createCreateUserRequest(username, password, email);
        var exception = new InvalidRequestException(debugMessage);

        doThrow(exception).when(authenticationService).createUser(username, password, email);
        var result = mockMvc.perform(post(AUTH_CREATE_USER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(request)));

        ControllerTestUtils.testErrorResponse(result, HttpStatus.BAD_REQUEST, ApiErrorType.INVALID_INPUT_ERROR, debugMessage);
    }

    public static GetAuthTokenRequest createGetAuthTokenRequest(String username, String password) {
        var request = new GetAuthTokenRequest();
        request.setUsername(username);
        request.setPassword(password);
        return request;
    }

    public static CreateUserRequest createCreateUserRequest(String username, String password, String email) {
        var request = new CreateUserRequest();
        request.setUsername(username);
        request.setPassword(password);
        request.setEmail(email);
        return request;
    }

}