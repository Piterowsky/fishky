package com.fishky.auth.validator;

import com.fishky.auth.error.exception.InvalidRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
class CreateUserRequestValidatorTest {

    @Mock
    private UsernameValidator usernameValidator;

    @Mock
    private PasswordValidator passwordValidator;

    @Mock
    private EmailValidator emailValidator;

    @InjectMocks
    private CreateUserRequestValidator createUserRequestValidator;

    @Test
    void validate_whenUsernameValidatorThrowsException_exceptionIsPassedAlong() {
        var test = "test";

        doThrow(InvalidRequestException.class).when(usernameValidator).validate(test);
        Executable executable = () -> createUserRequestValidator.validate(test, test, test);

        assertThrows(InvalidRequestException.class, executable);
    }

    @Test
    void validate_whenPasswordValidatorThrowsException_exceptionIsPassedAlong() {
        var test = "test";

        doThrow(InvalidRequestException.class).when(passwordValidator).validate(test);
        Executable executable = () -> createUserRequestValidator.validate(test, test, test);

        assertThrows(InvalidRequestException.class, executable);
    }

    @Test
    void validate_whenEmailValidatorThrowsException_exceptionIsPassedAlong() {
        var test = "test";

        doThrow(InvalidRequestException.class).when(emailValidator).validate(test);
        Executable executable = () -> createUserRequestValidator.validate(test, test, test);

        assertThrows(InvalidRequestException.class, executable);
    }

    @Test
    void validate_whenAnyValidatorThrowsException_doesNotThrowAnyException() {
        var test = "test";

        Executable executable = () -> createUserRequestValidator.validate(test, test, test);

        assertDoesNotThrow(executable);
    }

}