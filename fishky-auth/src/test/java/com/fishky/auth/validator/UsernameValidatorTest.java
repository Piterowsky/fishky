package com.fishky.auth.validator;

import com.fishky.auth.error.exception.InvalidRequestException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static com.fishky.auth.validator.UsernameValidator.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UsernameValidatorTest {

    @Spy
    private UsernameValidator usernameValidator;

    @ParameterizedTest
    @ValueSource(strings = {
            "testTEST1234",
            "testTEST",
            "testte",
            "TEST1234",
            "TESTTE",
            "test_TEST_1234",
    })
    void validate_whenGivenUsernameCorrect_doesNotThrowException(String username) {
        assertDoesNotThrow(() -> usernameValidator.validate(username));
    }

    public static Stream<Arguments> provider_validate_whenGivenUsernameCorrect_throwsException() {
        return Stream.of(
                Arguments.of("test", SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE),
                Arguments.of("t".repeat(MAX_SIZE + 1), SHOULD_HAVE_MAX_CHARACTERS_LENGTH_MESSAGE),
                Arguments.of("testTE%^T1234", NO_ILLEGAL_CHARACTERS_MESSAGE),
                Arguments.of("testTEST 1234", NO_WHITESPACE_CHARACTERS_MESSAGE)
        );
    }

    @ParameterizedTest
    @MethodSource("provider_validate_whenGivenUsernameCorrect_throwsException")
    void validate_whenGivenUsernameCorrect_throwsException(String username, String expectedMessage) {
        var ex = assertThrows(InvalidRequestException.class, () -> usernameValidator.validate(username));
        assertEquals(expectedMessage, ex.getMessage());
    }

}