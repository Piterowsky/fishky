package com.fishky.auth.validator;

import com.fishky.auth.error.exception.InvalidRequestException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static com.fishky.auth.validator.PasswordValidator.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PasswordValidatorTest {

    @Spy
    private PasswordValidator passwordValidator;

    @ParameterizedTest
    @ValueSource(strings = {
            "testTEST1234",
            "testTEST!@#$1234",
            "1234testTEST",
            "test!1234$$.. Test"
    })
    void validate_whenGivenPasswordCorrect_doesNotThrowException(String password) {
        assertDoesNotThrow(() -> passwordValidator.validate(password));
    }

    public static Stream<Arguments> provider_validate_whenGivenPasswordIncorrect_throwException() {
        return Stream.of(
                Arguments.of("tesT12", SHOULD_HAVE_MIN_CHARACTERS_LENGTH_MESSAGE),
                Arguments.of("testTESTtest", AT_LEAST_ONE_NUMBER_MESSAGE),
                Arguments.of("testtest1234", AT_LEAST_ONE_UPPERCASE_MESSAGE),
                Arguments.of("TEST1234TEST", AT_LEAST_ONE_LOWERCASE_MESSAGE)
        );
    }

    @ParameterizedTest
    @MethodSource("provider_validate_whenGivenPasswordIncorrect_throwException")
    void validate_whenGivenPasswordIncorrect_throwException(String password, String message) {
        var exception = assertThrows(InvalidRequestException.class, () -> passwordValidator.validate(password));

        assertEquals(message, exception.getMessage());
    }

}