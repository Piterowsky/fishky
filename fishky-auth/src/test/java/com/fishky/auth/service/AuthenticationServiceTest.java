package com.fishky.auth.service;

import com.fishky.auth.error.exception.InvalidRequestException;
import com.fishky.auth.error.exception.NotFoundException;
import com.fishky.auth.model.User;
import com.fishky.auth.repository.UserRepository;
import com.fishky.auth.util.JwtUtils;
import com.fishky.auth.validator.CreateUserRequestValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AuthenticationServiceTest {

    @Spy
    private JwtUtils jwtUtils;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private CreateUserRequestValidator createUserRequestValidator;

    @InjectMocks
    private AuthenticationService authenticationService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(jwtUtils, "tokenValidityInSeconds", 900);
        ReflectionTestUtils.setField(jwtUtils, "tokenSecret", "$3cr3t");
    }

    @Test
    void getAuthToken_whenThereIsNoUserWithGivenUsername_throwsNotFoundException() {
        var username = "username";

        doReturn(Optional.empty()).when(userRepository).findOneByUsername(username);
        Executable executable = () -> authenticationService.getAuthToken(username, null);

        var ex = assertThrows(NotFoundException.class, executable);
        assertEquals("User with given username does not exist", ex.getMessage());
    }

    @Test
    void getAuthToken_whenGivenPasswordIsInvalid_throwsInvalidRequestException() {
        var username = "username";
        var hash = "hash";
        var plainPassword = "plainPassword";
        var user = User.builder().username(username).hash(hash).build();

        doReturn(Optional.of(user)).when(userRepository).findOneByUsername(username);
        doReturn(false).when(passwordEncoder).matches(plainPassword, hash);
        Executable executable = () -> authenticationService.getAuthToken(username, plainPassword);

        var ex = assertThrows(InvalidRequestException.class, executable);
        assertEquals("Given credentials are incorrect", ex.getMessage());
    }

    @Test
    void getAuthToken_whenCorrect_returnToken() {
        var username = "username";
        var hash = "hash";
        var plainPassword = "plainPassword";
        var user = User.builder().username(username).hash(hash).build();

        doReturn(Optional.of(user)).when(userRepository).findOneByUsername(username);
        doReturn(true).when(passwordEncoder).matches(plainPassword, hash);

        var token = authenticationService.getAuthToken(username, plainPassword);
        assertTrue(jwtUtils.validateToken(token));
    }

    @Test
    void createUser_whenValidationErrors_throwException() {
        var username = "username";
        var password = "password";
        var email = "email";
        var expectedMessage = "message";
        var expectedException = new InvalidRequestException(expectedMessage);

        doThrow(expectedException).when(createUserRequestValidator).validate(username, password, email);
        Executable executable = () -> authenticationService.createUser(username, password, email);

        var ex = assertThrows(InvalidRequestException.class, executable);
        assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    void createUser_whenUserWithGivenUsernameAlreadyExists_throwException() {
        var username = "username";
        var password = "password";
        var email = "email";
        var expectedMessage = String.format("User with username '%s' already exists", username);

        doNothing().when(createUserRequestValidator).validate(username, password, email);
        doReturn(true).when(userRepository).existsByUsername(username);
        Executable executable = () -> authenticationService.createUser(username, password, email);

        var ex = assertThrows(InvalidRequestException.class, executable);
        assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    void createUser_whenUserWithGivenEmailAlreadyExists_throwException() {
        var username = "username";
        var password = "password";
        var email = "email";
        var expectedMessage = String.format("Email '%s' has already been used to register an account", email);

        doNothing().when(createUserRequestValidator).validate(username, password, email);
        doReturn(false).when(userRepository).existsByUsername(username);
        doReturn(true).when(userRepository).existsByEmail(email);
        Executable executable = () -> authenticationService.createUser(username, password, email);

        var ex = assertThrows(InvalidRequestException.class, executable);
        assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    void createUser_whenCorrect_returnCreatedUser() {
        var username = "username";
        var password = "password";
        var email = "email";
        var encodedPassword = "encoded-password";
        var userToSave = User.builder().username(username).hash(encodedPassword).email(email).build();

        doNothing().when(createUserRequestValidator).validate(username, password, email);
        doReturn(false).when(userRepository).existsByUsername(username);
        doReturn(false).when(userRepository).existsByEmail(email);
        doReturn(encodedPassword).when(passwordEncoder).encode(password);
        doReturn(userToSave).when(userRepository).save(userToSave);
        var savedUser = authenticationService.createUser(username, password, email);

        assertEquals(userToSave.getUsername(), savedUser.getUsername());
        assertEquals(userToSave.getPassword(), savedUser.getPassword());
        assertEquals(userToSave.getEmail(), savedUser.getEmail());
    }

}